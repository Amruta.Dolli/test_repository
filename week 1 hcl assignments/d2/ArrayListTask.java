package com.hclpackage.d2;

import java.util.*;

public class ArrayListTask {
	public static void main(String[] args) {

		// printing tasks
		int i;
		String s;
		Scanner taskname = new Scanner(System.in);
		System.out.print("Enter the name : ");
		s = taskname.nextLine();
		System.out.println("Name: " + s);
		System.out.println(" ");

		// printing 5 tasks
		Scanner sc = new Scanner(System.in);
		String[] task = new String[5];
		System.out.println("Enter the five tasks: ");
		for (i = 0; i < 5; i++) {
			task[i] = sc.nextLine();
		}
		System.out.println(" ");

		int arraylength = task.length;

		for (i = 0; i < arraylength; i++) {
			System.out.println("Task " + i + ": " + task[i]);

		}
		System.out.println(" ");

		// tasks to be shown in order
		String temp;
		for (i = 0; i < arraylength - 1; i++) {
			for (int j = 1; j < arraylength; j++) {
				if (task[j - 1].compareTo(task[j]) > 0) {
					temp = task[j - 1];
					task[j - 1] = task[j];
					task[j] = temp;
				}
			}

		}
		for (i = 0; i < arraylength; i++) {
			System.out.println(task[i]);

		}
		System.out.println(" ");

		// For repeated task
		for (i = 0; i < (task.length - 1); i++) {
			if (task[i].equals(task[i + 1])) {
				System.out.println("Repeated Task: " + task[i]);
			}
		}
		System.out.println(" ");

		// Updating the task in an array
		System.out.println("Updating the task: ");
		Scanner sss = new Scanner(System.in);
		System.out.print("Enter the index number where you have to update(0-4) : ");
		int num = sss.nextInt();
		Scanner newtask = new Scanner(System.in);
		System.out.println("Enter the New task : ");
		String updatedtask = newtask.nextLine();
		task[num] = updatedtask;
		System.out.println(" ");

		for (i = 0; i < arraylength; i++) {
			System.out.println(task[i]);

		}
		System.out.println(" ");

		// Deleting a task in a array
		System.out.println("Deleting the task: ");
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the index number where you have to delete(0-4) : ");
		int n1 = in.nextInt();

		System.out.println(" ");

		for (i = 0; i < arraylength; i++) {
			if (i == n1) {
				System.out.println(" ");
				continue;
			}
			System.out.println(task[i]);

		}
		System.out.println(" ");

		// Searching a task from array
		System.out.println("Searching a task: ");
		Scanner searchtask = new Scanner(System.in);
		System.out.print("Enter the task : ");
		String tt = searchtask.nextLine();
		System.out.println("");

		for (i = 0; i < task.length; i++) {
			if (tt.equals(task[i])) {
				System.out.println("Task found at index " + i);

			}
		}

	}
}