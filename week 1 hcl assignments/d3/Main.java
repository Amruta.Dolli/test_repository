package com.hclpackage.d3;

import com.hclpackage.d41.Car;

public class Main {

	public static void main(String[] args) {

		Vehicle vehicle = new Vehicle();

		Vehicle scooter = new Scooter();

		Vehicle car = new Car();

		vehicle.noOfWheels();

		scooter.noOfWheels();

		car.noOfWheels();

	}

}
