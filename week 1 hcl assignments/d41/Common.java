package com.hclpackage.d41;

public interface Common {
	public String markAttendance();

	public String dailyTask();

	public String displayDetails();

}

class Employee implements Common {
	public String markAttendance() {
		System.out.println("attendence marked for today");
        return null;
	}

	public String dailyTask() {
		System.out.println("complete coding of module 1");
		return null;
	}

	public String displayDetails() {
	System.out.println("employee details");
	return null;
	}

}

class Manager implements Common {
	public String markAttendance() {
		System.out.println("Attendence marked today");
		return null;

	}

	public String dailyTask() {
		System.out.println("create project architecture");
		return null;
	}

	public String displayDetails() {
		System.out.println("manager details");
		return null;
	}

}
