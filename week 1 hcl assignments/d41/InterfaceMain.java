package com.hclpackage.d41;

public class InterfaceMain {
	public static void main(String[] args) {

		Employee emp = new Employee();

		emp.dailyTask();

		emp.markAttendance();

		emp.displayDetails();

		Manager manager = new Manager();

		manager.markAttendance();

		manager.dailyTask();

		manager.displayDetails();

	}

}
