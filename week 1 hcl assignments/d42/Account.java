package com.hclpackage.d42;

public class Account {
	String customerName;
	int accountNo;

	Account(String a, int b)

	{
		customerName = a;
		accountNo = b;
	}

	void display()

	{
		System.out.println("customeName is " + customerName);
		System.out.println("Accountno is " + accountNo);

	}

}

class CurrentAccount extends Account

{

	int currentBalance;

	CurrentAccount(String a, int b, int c)

	{

		super(a, b);

		currentBalance = c;

	}

	void display()

	{

		super.display();

		System.out.println("Current Balance: " + currentBalance);

	}

}

class AccountDetails extends CurrentAccount

{
	int depositAmount;
	int withdrawlAmount;

	AccountDetails(String a, int b, int c, int d, int e)

	{
		super(a, b, c);
		depositAmount = d;
		withdrawlAmount = e;

	}

	void display()

	{
		super.display();
		System.out.println("depositAmount is " + depositAmount);
		System.out.println("withdraw amount is " + withdrawlAmount);
	}

}
